#!/usr/bin/python3

import argparse
import logging
from concurrent.futures import ThreadPoolExecutor
from typing import Optional

import pydicom
from requests import delete

from dicom_utils import get_json, get_server_from_host
from dicom_utils.archive import connect, find_in_archive
from dicom_utils.dcm import difference
from dicom_utils.server import Server


def diff_instance(a_ds: pydicom.Dataset, server: Server, instance_id: str, ignore: list) -> Optional[tuple[list, list]]:
	"""
	Checks if data in the archive is missing stuff the db has.
	Data the archive has but not the db are ignored here. As are data given in the ignore-list.
	If the data are considered equal (aka the funktion returns two empty lists), the instance is marked as "archived" in the database
	:param a_ds: archive data
	:param server: db server to look for the instance
	:param instance_id: the instance id
	:param ignore: list of data to be ignored when generating the difference
	:return: (data missing in the archive, list of data pairs different between db and archive) or None if data are considered equal
	"""
	(a_miss, diff, db_miss) = difference(a_ds, server.download_ds(instance_id), ignore)
	# if diff:
	# 	logging.warning(f'Some data in {server}/instances/{instance_id} differ from the same data in the archive')
	# 	for d in diff:
	# 		logging.info(d)
	# 	return False
	# if a_miss:
	# 	logging.warning(f'Some data in {server}/instances/{instance_id} are missing in the archive')
	# 	for d in a_miss:
	# 		logging.info(d)
	# 	return False

	if db_miss and not (a_miss or diff):
		logging.info(f'{server}/instances/{instance_id} is missing some data, but letting it slide as the archive is complete\n{db_miss}')

	if a_miss or diff:
		return (a_miss, diff)
	else:
		server.set_archived(instance_id,set=True)
		return None


def diff_study(server: Server, db_instances, dicom_source, ignore: list) -> (list, list, dict):
	"""
	Checks if the given study is properly archived.

	Will look for a tar-file for the date/time of the study, extract all dicom files, look for the respective entries
	in the db and if found compare them.
	:param server: the db-server to connect to
	:param db_instances: list of database instances expected to be found in the archive
	:param dicom_source: generator yielding the dicoms from the archive
	:param ignore: list of dicom tags to be ignored when generating the differences
	:return: ("missing in the archive (ID)","missing in the db (file path)","dict(instance:(result of diff_instance))"
	)
	"""
	missing_in_db = []
	futures = {}
	with ThreadPoolExecutor() as executor:
		for (a_tarinfo, a_ds) in dicom_source:
			if a_ds.SOPInstanceUID in db_instances:  # file is in list of expected files, diff them
				instance = db_instances.pop(a_ds.SOPInstanceUID)
				futures[instance["ID"]] = executor.submit(
					diff_instance, a_ds=a_ds, server=server, instance_id=instance["ID"], ignore=ignore
				)
			else:  # file is NOT in list of expected files, try to find it
				fallback = server.lookup_instance(uid=a_ds.SOPInstanceUID)
				if fallback:  # ok found it elsewhere, diff that
					futures[fallback["ID"]] = executor.submit(
						diff_instance, a_ds=a_ds, server=server, instance_id=fallback, ignore=ignore
					)
				else:
					missing_in_db.append(a_tarinfo.path)  # otherwise, note it down as missing

		diffs = {key: future.result() for key, future in futures.items() if future.result()}

	return db_instances, missing_in_db, diffs


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='check archive for data')
	parser.add_argument('studyid', nargs='*')
	parser.add_argument('-d', '--host', help='orthanc db host', default='http://cameron:2023')
	parser.add_argument('-l', '--login', help='login for the ssh connection to the archive', default="reimer@hsm")
	parser.add_argument('--apath', nargs="*", help='archive search paths', default=[
		'/grauserv-ro/raw_data/mr/probands/2024',
		'/grauserv-ro/raw_data/mr/probands/2023',
		'/grauserv-ro/raw_data/mr/probands/2022',
		'/grauserv-ro/raw_data/mr/probands/2021',
		'/grauserv-ro/raw_data/mr/probands/2020',
		'/grauserv-ro/raw_data/mr/probands/2019',
		'/grauserv-ro/raw_data/mr/probands/2018',
		'/grauserv-ro/raw_data/mr/probands/2017',
		'/grauserv-ro/raw_data/mr/probands/2016',
		'/grauserv-ro/raw_data/mr/probands/2015',
		'/grauserv-ro/raw_data/mr/probands/2014'
	])
	parser.add_argument('-k', '--keyfile', help="private rsa keyfile for login", default=None)
	parser.add_argument('-f', '--filter', help="suffix for the regex filter for archive files to consider",
	                    default=r'(\.[\w]+)?\.tar\.\w+$')
	parser.add_argument('--log-level', help='logging level (CRITICAL,FATAL,ERROR,WARNING,INFO,DEBUG)',
	                    default='warning')
	parser.add_argument('--archived', help="also check instances which already are marked as archived",
	                    action='store_true', default=False)
	parser.add_argument('--delete', help="delete archived studies", action='store_true', default=False)
	parser.add_argument('--ignore', help='dicom tags to ignore when comparing db to archive', default=[
		"StudyDescription", "PatientName", "PatientID", "PatientBirthDate", "WindowCenter", "WindowWidth",
		(0x0028, 0x1055),
		(0x0038, 0x0010),
		(0x0029, 0x1210),
		(0x0029, 0x1110)  # MedCom OOG Info
	])

	args = parser.parse_args()
	logging.basicConfig(level=args.log_level.upper())
	logging.getLogger('paramiko.transport').setLevel('WARNING')
	logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

	connection = connect(args.login, keyfile=args.keyfile)
	server = get_server_from_host(args.host)
	studies = args.studyid if args.studyid else server.get_json(f'/studies')

	for study_id in studies:
		db_instances = server.list_instances(f'/studies/{study_id}')

		if not db_instances:
			logging.info(f'no relevant instances found in {study_id}, skipping..')
			continue

		(missing_in_a, missing_in_db, differences) = ([], [], {})
		if args.archived or next(filter(lambda v: not server.is_archived(v['ID']), db_instances.values()), None):
			study_meta = server.get_json(f'/studies/{study_id}')
			archive_dicoms = find_in_archive(study_meta, paths=args.apath, connection=connection, filter_suffix=args.filter)
			if not archive_dicoms:
				logging.warning(f'no archive file found for {server.get_study_name(study_meta)} ({server}/studies/{study_id})')
				continue

			(missing_in_a, missing_in_db, differences) = \
				diff_study(server, db_instances=db_instances, dicom_source=archive_dicoms, ignore=args.ignore)
			if missing_in_a:
				logging.warning(
					f'the archive is missing {len(missing_in_a.keys())} instances of {args.host}/studies/{study_id}')
				logging.info("\n".join(missing_in_a.keys()))
			if missing_in_db:
				logging.warning(
					f'the {args.host}/studies/{study_id} is missing {len(missing_in_db)} files found in the archive')
				for m in missing_in_db: logging.info(m)
			for key, diff in differences.items():
				if len(diff.keys()):
					logging.warning(f'the instance {key} of {study_id} has {len(diff.keys())} differences')
				for d in diff.items():
					logging.info(f'{d}')

		if args.delete:
			if not (missing_in_a or missing_in_db or differences):
				logging.warning(f'{args.host}/studies/{study_id} is fully archived, deleting as requested..')
				response = delete(f'{args.host}/studies/{study_id}')
				if response.status_code != 200:
					response.raise_for_status()
			else:
				logging.warning(f"{args.host}/studies/{study_id} is not fully archived, won't delete..")
		elif not (missing_in_a or missing_in_db or differences):
			logging.info(f'{args.host}/studies/{study_id} is fully archived, all good..')
