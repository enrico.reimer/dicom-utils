import requests.exceptions

from .core import *

def get_server_from_host(host:str):
	from .orthanc import Orthanc
	from .rudicom import Rudicom, is_rudicom
	return Rudicom(host) if is_rudicom(host) else Orthanc(host)

def get_server_from_url(url:str):
	if m := re.match("http://[^/]+",url):
		return get_server_from_host(m[0])
	else:
		raise requests.exceptions.InvalidURL