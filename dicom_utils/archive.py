import grp
import logging
import os
import pwd
import tarfile as tar
from datetime import time
from io import BytesIO
from os import path
from re import match
from typing import Optional, Tuple
import time

from paramiko import Transport, SFTPClient, RSAKey
from pydicom import dcmread, errors as dc_errors, dcmwrite, FileDataset

from . import get_server_from_host


def connect(login, keyfile=None):
	"""
	open sftp connection
	:param login: login string username@host
	:param keyfile: private key file (will be used for passwordless login if given)
	:return:
	"""
	(user, host) = login.split("@")
	ssh = Transport((host, 22))

	if keyfile:
		pkey = RSAKey.from_private_key_file(keyfile)
		ssh.connect(username=user, pkey=pkey)
	else:  # try GSS Authentication
		ssh.connect(username=user, gss_auth=True)
	return ssh


def get_file_list(path, connection: Transport, regex=None):
	"""
	list all files in an archive file
	:param path: path to the archive file
	:param connection: ssh connection to the archive
	:param regex: string to filter results using re.match
	:return: list of file names inside the archive file
	"""
	list = SFTPClient.from_transport(connection).listdir(path=path)
	if not regex:
		return list
	else:
		return filter(lambda f: match(regex, f), list)


def find_in_archive(study_meta: dict, paths: list, connection: Transport, filter_suffix: str):
	"""
	find an archive file matching given study metadata
	This works by looking for files that contain the StudyDate, StudyTime plus the filter suffix.
	:param study_meta: metadata to look for (needs to be a dict containing "StudyDate" and "StudyTime")
	:param paths: directory paths in the archive to look for the file in
	:param connection: ssh connection to the archive
	:param filter_suffix: file suffix (e.g. ".tar.xz")
	:return:
	"""
	assert ('MainDicomTags' in study_meta)
	main_tags = study_meta['MainDicomTags']
	(date_string, time_string) = (main_tags['StudyDate'][2:], main_tags['StudyTime'][:6])
	search_string = f'[a-zA-Z0-9\\._]+({date_string}_{time_string})' + filter_suffix
	return find_regex_in_archive(search_string, paths, connection, study_meta["ID"])


def find_regex_in_archive(search_string: str, paths: list, connection: Transport, name: Optional[str] = None):
	"""
	find an archive file matching given filename regex
	:param search_string: filename regex to look for
	:param paths: directory paths in the archive to look for the file in
	:param connection: ssh connection to the archive
	:param name: the name of what you're looking for (for error message purposes only)
	:return:
	"""
	for apath in paths:
		logging.debug(f'looking for {search_string} in the archive path {apath}')
		found = list(get_file_list(path=apath, connection=connection, regex=search_string))
		if len(found):
			logging.info(f'found study {name if name else search_string} in {apath}/{found[0]}')
			return extract_dicom(connection=connection, filename=path.join(apath, found[0]))
	logging.info(f'{search_string} was not found')
	return None


def extract(connection, filename):
	"""
	Generator to extract file objects from an archive file
	:param connection: ssh connection to the archive
	:param filename: path of the archive file
	:return: list of pairs of TarInfo, IO[bytes]
	"""
	pipe = SFTPClient.from_transport(connection, window_size=2 ** 24).open(filename)
	tarfile = tar.open(mode='r:*', fileobj=pipe)
	for tarobj in tarfile:
		if tarobj.isfile():
			yield tarobj, tarfile.extractfile(tarobj)


def extract_dicom(connection, filename):
	"""
	Generator to extract dicom objects from an archive file.
	Files inside the archive file that are not dicom will be skipped.
	:param connection: ssh connection to the archive
	:param filename: path of the archive file
	:return: list of pairs of TarInfo, pydicom.FileDataset
	"""
	for (info, obj) in extract(connection, filename):
		if info.name.split('/')[-1] in ["md5sum", "_differences.txt", "_errors.txt"]:
			continue

		try:
			yield info, dcmread(obj)
		except dc_errors.InvalidDicomError:
			logging.info("Failed to read {} from {} as dicom".format(info.name, filename))


def store_ds(tarfile: tar.TarFile, rootpath: str, data: FileDataset):
	"""
	Stores a single dicom dataset in an TarFile
	:param tarfile: the tarfile to write to
	:param rootpath: the directory where the file should be **inside** the tar file
	:param data: the dicom dataset
	"""
	#create buffer
	buffer = BytesIO()
	dcmwrite(buffer, data)

	#synthesize tarinfo
	tarinfo = tarfile.tarinfo()
	tarinfo.type = tar.REGTYPE
	tarinfo.name = f'{rootpath}/{data.SOPInstanceUID}.dcm'
	tarinfo.mode = 0o0200 | 0o0400 | 0o0020 | 0o0040
	tarinfo.uid = os.getuid()
	tarinfo.gid = os.getgid()
	tarinfo.size = buffer.tell()
	tarinfo.mtime = time.time()
	tarinfo.uname = pwd.getpwuid(tarinfo.uid)[0]
	tarinfo.gname = grp.getgrgid(tarinfo.gid)[0]

	logging.debug(f'storing {tarinfo.name}')
	buffer.flush()
	buffer.seek(0)
	tarfile.addfile(tarinfo, buffer)


def store(url: str, archive_dir: str, suffix: str, connection, override: bool = False) -> Optional[Tuple[int,str]]:
	"""
	Stores a given study in the archive.

	The archive file name is created as f'{subject_id}_{StudyDate}_{StudyTime}{suffix}.tar.xz'
	:param url: full url to the study including protocol and server
	:param archive_dir: directory on the archive to store the archive file in
	:param suffix: suffix to be added to the archive file name
	:param connection: ssh connection to the archive
	:param override: if already existing archive files should be overwritten (default is to abort)
	:return: tuple of amount of stored instances and the path of the created archive file or None if store is aborted
	"""
	(server, table, stdy_id) = split_url(url)
	server = get_server_from_host(server)
	stdy_name = server.get_study_name(f'/{table}/{stdy_id}')
	if not stdy_name:
		logging.critical(f'failed to get name of {url}')
		return None
	archive_name = f'{stdy_name}{suffix}.tar.xz'

	if not override:
		if len(list(get_file_list(path=archive_dir, connection=connection, regex=archive_name))):
			logging.info(f'{archive_dir}/{archive_name} already exists, skipping ..')
			return None

	with (SFTPClient.from_transport(connection, window_size=2 ** 24)
			      .open(f'{archive_dir}/{archive_name}', 'w' if override else 'wx') as sink):
		with tar.open(mode='w|xz', fileobj=sink) as tarfile:
			instances = server.list_instances(f'/{table}/{stdy_id}')
			instances = [i["ID"] for i in instances.values()]
			logging.debug(f'writing {len(instances)} to {archive_dir}/{archive_name}')
			for i in instances:
				ds = server.download_ds(i)
				store_ds(tarfile=tarfile, rootpath=stdy_name, data=ds)
			return len(instances), f'{archive_dir}/{archive_name}'
