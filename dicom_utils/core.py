import logging
import re
from typing import Optional

from requests import request as http_request
from pathlib import Path

_name_map = dict()
_codes = set()

_hscy_token = open( Path.home() /  ".huscy_token").readline().strip() if Path.exists(Path.home() /  ".huscy_token") else None

# make a REST request - returns json decoded as map
def get_json(url, method='get', data=None) -> Optional[dict]:
	r"""Make a rest request, interpret result as json.

	:param data: data for POST or PUT requests
	:param method: request method (default='get')
	:param url: the url for the request
	"""
	response = http_request(method=method, url=url, data=data)

	if response.status_code == 404 and method.upper() == 'GET':
		return None  #thats kinda ok on GET
	if response.status_code >= 400:
		response.raise_for_status()

	return response.json() if response.content else None


_study_map = dict()
def _populate_study_map():
	if _study_map: return
	with open("/data/p_bdb/prob_map/study_map.txt", 'rb') as file:
		next(file)  # skip header
		for line in file:
			try:
				(v, k) = line.strip().decode().split('|')
				v = v.strip()
				if k:
					_study_map[k] = v
			except UnicodeDecodeError:
				None  # logging.debug(b'Failed to decode '+line.strip())

def is_study(stdy_name:str):
	_populate_study_map()
	return stdy_name in _study_map

def _populate_name_map():
	if _name_map: return
	for map_file in ['/data/p_bdb/prob_map/prob_mapmerge.txt']:
		with open(map_file, 'rb') as file:
			next(file)  # skip header
			for line in file:
				try:
					(v, k) = line.strip().decode().split('|')
					v = v.strip()
					if k != "_NULL_":
						_name_map[k] = v
					_codes.add(v)
				except UnicodeDecodeError:
					None  # logging.debug(b'Failed to decode '+line.strip())


def get_huscy(path:str) -> Optional[dict]:
	if _hscy_token is None:
		return None

	response = http_request("get", "https://huscy.cbs.mpg.de"+path, headers={"Authorization": f"Token {_hscy_token}"})

	if response.status_code == 404:
		return None  #thats kinda ok on GET
	if response.status_code >= 400:
		response.raise_for_status()

	return response.json() if response.content else None

def match_code(code: str) -> bool:
	hscy = get_huscy(f"/api/mpicbs/subjects/{code}")
	if hscy:
		return hscy["pseudonym"]
	else:
		_populate_name_map()
		return code in _codes



def match_4letter(patient_name: str) -> Optional[str]:
	fourletter_code = re.compile(r'(\w\w[\d\w][TXP])($|\d+)')
	match = fourletter_code.match(patient_name)
	if match:
		patient_name = match[1]
		_populate_name_map()
		if patient_name in _name_map:
			return _name_map[patient_name]
		else:
			logging.debug(f"4-letter code '{patient_name}' wasn't found")
	return None
