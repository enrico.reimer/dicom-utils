import gzip
import logging
import zlib
from io import BytesIO
from typing import Optional

import pydicom
from pydicom.tag import Tag

from . import is_study

def read(file, compression=None) -> pydicom.FileDataset:
	if compression == 'gz':
		file = gzip.open(file, 'rb')
	elif compression == 'orthanc':
		compressed = open(file, 'rb').read()[8:]
		if compressed[0] != 0x78:
			logging.critical("Unrecognized orthanc compression")
			raise NotImplemented
		file = BytesIO(zlib.decompress(compressed))
	return pydicom.dcmread(file)


def _half_diff(src: pydicom.Dataset, compare: pydicom.Dataset, ignore: list[Tag]) -> (list, list):
	"""
	Get a list of data from src which are missing in or different from compare
	:param src: set of data to look for
	:param compare: set of data to look in
	:return: (set of data missing in compare, set difference pairs)
	"""
	if ignore is None:
		ignore = []
	missing = []
	diff = []
	for elem in src:
		if elem.tag in ignore:
			continue
		if elem.tag not in compare:
			missing.append(elem)
		elif compare[elem.tag] != elem:
			diff.append((elem, compare[elem.tag]))
	return missing, diff


def difference(data: pydicom.Dataset, compare: pydicom.FileDataset, ignore:list[Tag]) -> (list, list, list):
	"""
	Get a list of data from data and the "supposed" duplicate
	:param data: reference dataset
	:param compare: compared dataset
	:return: (set of data missing in data, set of data different in both, set of data missing in compare)
	"""
	(missing_in_data, _) = _half_diff(src=compare, compare=data, ignore=ignore)
	(missing_in_compare, diff) = _half_diff(src=data, compare=compare, ignore=ignore)

	return missing_in_data, diff, missing_in_compare


def anonymize_ds(ds: pydicom.FileDataset, override_subjectname:Optional[str] = None, override_study_id:Optional[str] = None):
	assert isinstance(ds, pydicom.Dataset)

	if override_subjectname:
		# if PatientID is actually a known studyid save it as StudyID
		if is_study(ds[0x00100020].value):
			ds[0x00200010].value = ds[0x00100020].value

		ds[0x00100010].value = override_subjectname
		ds[0x00100020].value = override_subjectname

	if override_study_id:
		ds[0x00200010].value = override_study_id

	if 'PatientBirthDate' in ds:
		del ds[0x00100030]

	return ds
