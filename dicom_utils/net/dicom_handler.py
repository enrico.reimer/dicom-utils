from datetime import datetime
from pydicom.dataset import Dataset

from pynetdicom import AE, debug_logger, evt, build_role
from pynetdicom._globals import BIND_ADDRESS
from pynetdicom.sop_class import StudyRootQueryRetrieveInformationModelFind, MRImageStorage, \
	StudyRootQueryRetrieveInformationModelGet, EnhancedSRStorage


class UnsupportedSOPClassError(Exception):
	""" this exception is thrown when the user tries to fetch an instance that has an unsupported SOP Class UID """

	def __init__(self, sop_class_uid, *args, **kwargs):
		"""
		:param sop_class_uid: ID of the unsupported SOP Class
		"""

		self._sop_class_uid = sop_class_uid
		super(UnsupportedSOPClassError, self).__init__(*args, **kwargs)

	def get_sop_class_uid(self):
		""" returns the ID of the SOP Class that is not supported """

		return self._sop_class_uid


def ensure_association(func):
	""" decorator used to ensure an active dicom connection """

	def inner(*args, **kwargs):
		instance = args[0]
		if not instance._association or not instance._association.is_established:
			# associate
			instance._association = instance._association_entity.associate(
				instance._server,
				instance._port,
				bind_address=instance._bind_address,
				ae_title=instance._remote_ae_title,
				ext_neg=[instance._role],
				evt_handlers=instance._handlers
			)
		if not instance._association.is_established:
			raise ConnectionAbortedError(
				f'Failed to associate with server {instance._server}:{instance._port} '
				f'(remote AE: {instance._remote_ae_title}) as AE {instance._local_ae_title}'
			)

		# DIRTY HACK ;-)
		# the scanner does not tell the client that the Storage SCP role has been accepted but sends the DICOMs anyway
		# we have to modify the accepted contexts accordingly to prevent our SCP from rejecting the C-STORE request
		for context in instance._association._accepted_cx.values():
			if context.abstract_syntax in instance._storage_contexts:
				context._as_scp = True

		return func(*args, **kwargs)

	return inner


class DicomHandler:

	# this the only way to get access to datasets received via C-STORE
	_c_store_buffer = []

	# only instances with one of the following SOP Class IDs can be fetched
	# if other types have to be fetched, it's sufficient to add their context ID to this list
	_supported_storage_contexts = [
		MRImageStorage,
		EnhancedSRStorage,
		#        SecondaryCaptureImageStorage,
		'1.3.12.2.1107.5.9.1'  # Syngo Non Image Storage
	]

	def __init__(self, server, port, local_ae_title, remote_ae_title,
	             default_logging=False,
	             bind_address = BIND_ADDRESS,
	             storage_contexts=_supported_storage_contexts
	             ):
		"""
		:param server: server name or ip
		:param port: dicom server port
		:param local_ae_title: name of the local Association Entity
		:param remote_ae_title: name of the remote Association Entity
		:param default_logging: run debug_logger() of pynetdicom library
		:param storage_contexts: list of storage contexts that will be negotiated when establishing the association
		"""

		self._association = None
		self._server = server
		self._port = port
		self._bind_address=bind_address
		self._local_ae_title = local_ae_title
		self._remote_ae_title = remote_ae_title
		if default_logging:
			debug_logger()
		self._association_entity = AE(local_ae_title)

		# add all contexts we need for the communication
		self._association_entity.add_requested_context(StudyRootQueryRetrieveInformationModelFind)
		self._association_entity.add_requested_context(StudyRootQueryRetrieveInformationModelGet)
		self._storage_contexts = storage_contexts
		for storage_context in self._storage_contexts:
			self._association_entity.add_requested_context(storage_context)

		# create role and handler needed to handle C-STORE requests
		self._handlers = [(evt.EVT_C_STORE, DicomHandler._handle_c_store)]
		# if we do not also set scu_role=True, the scanner will reject the presentation context
		self._role = build_role(MRImageStorage, scp_role=True, scu_role=True)

	@staticmethod
	def _parse_timestamp(date, time):
		""" this function tries to create a datetime.datetime object from a DICOM date and time string

		:param date: DICOM date string
		:param time: DICOM time string
		:return: datetime.datetime or None
		"""

		if not date:
			return
		elif time:
			return datetime.strptime(date + time, '%Y%m%d%H%M%S.%f')
		else:
			return datetime.strptime(date, '%Y%m%d')

	@ensure_association
	def _c_find(self, dataset, root_context, timeout):
		"""
		sends a C-FIND request to retrieve a list of matching elements; raises ConnectionAbortedError

		:param dataset: dataset which defines the query
		:param root_context: root_context for the query
		:param timeout: DIMSE timeout
		:return: list of datasets which were returned
		"""

		self._association.dimse_timeout = timeout

		results = []
		for (status, identifier) in self._association.send_c_find(dataset, root_context):
			if not status:
				raise ConnectionAbortedError('Connection was aborted or received invalid response')

			# match was returned
			if status.Status in (65280, 65281):
				results.append(identifier)
			# success response
			elif status.Status == 0:
				continue
			else:
				raise ConnectionAbortedError(status.Status)

		return results

	@staticmethod
	def _handle_c_store(event):
		""" handler for C-STORE requests """

		dataset = event.dataset
		# add transfer syntax to the metadata
		dataset.ensure_file_meta()
		dataset.file_meta.TransferSyntaxUID = event.context.transfer_syntax
		dataset.fix_meta_info()

		DicomHandler._c_store_buffer.append(dataset)

		return 0x0000

	def get_studies(self, timeout=30):
		""" queries a list of list off all studies

		:param timeout: DIMSE timeout in seconds, should be increased when querying many items
		:return:    {
						'StudyInstanceUID_1': {
							'patient_id': 'PatientID',
							'timestamp': datetime.datetime or None,
							'study_id': 'StudyID_1'
						},
						'StudyInstanceUID_2': ...
					}
		"""

		studies = {}

		ds = Dataset()
		ds.StudyID = '*'
		ds.StudyInstanceUID = ''
		ds.PatientID = ''
		ds.PatientName = ''
		ds.StudyTime = ''
		ds.StudyDate = ''
		ds.QueryRetrieveLevel = 'STUDY'

		for x in self._c_find(ds, StudyRootQueryRetrieveInformationModelFind, timeout):
			assert x.StudyInstanceUID not in studies
			studies[x.StudyInstanceUID] = {
				'patient_id': x.PatientID,
				'patient_name': x.PatientName,
				'study_id': x.StudyID,
				'timestamp': DicomHandler._parse_timestamp(x.StudyDate, x.StudyTime)
			}

		return studies

	def get_series_for_study(self, study_instance_uid, timeout=30):
		""" queries all series belonging to the given study

		:param study_instance_uid: StudyInstanceUID
		:param timeout: DIMSE timeout in seconds, should be increased when querying many items
		:return:    {
						'SeriesInstanceUID_1': {
							'patient_id': 'PatientID_1',
							'timestamp': datetime.datetime or None,
							'parent_id': 'StudyInstanceUID'
						},
						'SeriesInstanceUID_2': ...
					}
		"""

		series = {}

		ds = Dataset()
		ds.StudyInstanceUID = study_instance_uid
		ds.SeriesInstanceUID = ''
		ds.SeriesTime = ''
		ds.SeriesDate = ''
		ds.PatientID = ''
		ds.QueryRetrieveLevel = 'SERIES'
		for x in self._c_find(ds, StudyRootQueryRetrieveInformationModelFind, timeout):
			assert x.SeriesInstanceUID not in series
			series[x.SeriesInstanceUID] = {
				'patient_id': x.PatientID,
				'timestamp': DicomHandler._parse_timestamp(x.SeriesDate, x.SeriesTime),
				'parent_id': x.StudyInstanceUID
			}

		return series

	def get_instances_for_series(self, study_instance_uid, series_instance_uid, timeout=30):
		""" queries metadata for all instances that belong to the given SeriesInstanceUID

		:param study_instance_uid: StudyInstanceUID (the scanner needs both IDs)
		:param series_instance_uid: SeriesInstanceUID
		:param timeout: DIMSE timeout in seconds, should be increased when querying many items
		:return:    {
						'SOPInstanceUID_1': {
							'timestamp': datetime.datetime or None,
							'sop_class_uid': SOPClassUID,
							'parent_id': SeriesInstanceUID
						},
						'SOPInstanceUID_2': ...
					}
		"""

		instances = {}

		ds = Dataset()
		ds.SeriesInstanceUID = series_instance_uid
		ds.StudyInstanceUID = study_instance_uid
		ds.SOPInstanceUID = ''
		ds.SOPClassUID = ''
		ds.InstanceCreationDate = ''
		ds.InstanceCreationTime = ''
		ds.QueryRetrieveLevel = 'IMAGE'

		for x in self._c_find(ds, StudyRootQueryRetrieveInformationModelFind, timeout):
			assert x.SOPInstanceUID not in instances
			instances[x.SOPInstanceUID] = {
				'timestamp': DicomHandler._parse_timestamp(
					x.InstanceCreationDate if hasattr(x, 'InstanceCreationDate') else '',
					x.InstanceCreationTime if hasattr(x, 'InstanceCreationTime') else ''
				),
				'sop_class_uid': x.SOPClassUID,
				'parent_id': x.SeriesInstanceUID
			}

		return instances

	@ensure_association
	def get_instance(self, study_instance_uid, series_instance_uid, sop_instance_uid, timeout=30):
		""" queries metadata for one specific instance

		:param study_instance_uid: StudyInstanceUID that belongs to the instance
		:param series_instance_uid: SeriesInstanceUID that belongs to the instance
		:param sop_instance_uid: sop_instance_uid
		:param timeout: DIMSE timeout in seconds
		:return:    {
						'sop_instance_uid': SOPInstanceUID,
						'sop_class_uid': SOPClassUID,
						'timestamp': datetime.datetime or None,
						'parent_id': SeriesInstanceUID
					}
					or None if item was not found
		"""

		ds = Dataset()
		ds.SeriesInstanceUID = series_instance_uid
		ds.StudyInstanceUID = study_instance_uid
		ds.SOPInstanceUID = sop_instance_uid
		ds.SOPClassUID = ''
		ds.InstanceCreationDate = ''
		ds.InstanceCreationTime = ''
		ds.QueryRetrieveLevel = 'IMAGE'

		result = self._c_find(ds, StudyRootQueryRetrieveInformationModelFind, timeout)
		if len(result) == 1:
			return  {
				'timestamp': DicomHandler._parse_timestamp(
					result[0].InstanceCreationDate if hasattr(result[0], 'InstanceCreationDate') else '',
					result[0].InstanceCreationTime if hasattr(result[0], 'InstanceCreationTime') else ''
				),
				'sop_instance_uid': result[0].SOPInstanceUID,
				'sop_class_uid': result[0].SOPClassUID,
				'parent_id': result[0].SeriesInstanceUID
			}
		elif len(result) > 1:
			raise RuntimeError('More than one instance matches the given SOPInstanceUID')

	@ensure_association
	def fetch_instances(self, study_instance_uid, series_instance_uid, sop_instance_uid, timeout=30):
		""" fetches instances from the DICOM server via C-GET

		:param study_instance_uid:  StudyInstanceUID that belongs to the instance
		:param series_instance_uid: SeriesInstanceUID that belongs to the instance
		:param sop_instance_uid: single sop_instance_id or a list of IDs
		:param timeout: DIMSE timeout in seconds, should be increased when querying many instance
						e.g. 120 seconds for 100 items
		:return: single pynetdicom.Dataset if a single ID was passed to the function or list of Datasets if multiple
				 IDs were given
		"""

		self._association.dimse_timeout = timeout

		# create a new list to remove data from former C-GET calls
		DicomHandler._c_store_buffer = []

		# check if multiple IDs were given
		single_id = False
		if not isinstance(sop_instance_uid, list):
			single_id = True
			sop_instance_uid = [sop_instance_uid]

		ds = Dataset()
		ds.StudyInstanceUID = study_instance_uid
		ds.SeriesInstanceUID = series_instance_uid
		ds.SOPInstanceUID = sop_instance_uid
		ds.QueryRetrieveLevel = 'IMAGE'
		for status, identifier in self._association.send_c_get(ds, StudyRootQueryRetrieveInformationModelGet):
			if not status:
				raise ConnectionAbortedError('Connection was aborted or received invalid response (you might try to increase the timeout)')

			# operation successfully finished / match was with(-out) warning returned
			if status.Status in (0, 65280, 65281):
				continue
			elif status.Status == 45056:
				instance = self.get_instance(study_instance_uid, series_instance_uid, sop_instance_uid)
				if instance['sop_class_uid'] not in self._storage_contexts:
					raise UnsupportedSOPClassError(instance['sop_class_uid'], f'Unable to fetch instance with unknown SOPClassUID: {instance["sop_class_uid"]}')
			else:
				raise RuntimeError(f'Failed to fetch instances with status {status.Status}')

		return \
			DicomHandler._c_store_buffer[0] \
				if len(DicomHandler._c_store_buffer) == 1 and single_id \
				else DicomHandler._c_store_buffer

	def disconnect(self):
		""" terminate dicom session """

		self._association.release()