import logging
import re
from datetime import datetime
from io import BytesIO
from os import path, mkdir
from typing import Optional, Union

import pydicom
from pydicom.uid import ExplicitVRLittleEndian
from requests import request as http_request

from . import get_json, get_server_from_url
from .server import Server

_url_matcher = re.compile(r'(http://[^/]+)/(\w+)/([a-f\d]{8}-[a-f\d]{8}-[a-f\d]{8}-[a-f\d]{8}-[a-f\d]{8})',
                          re.IGNORECASE)


class Orthanc(Server):
	def verify_request(self, instance_id: str):
		return http_request(method='post', url=self.fullpath(f'/instances/{instance_id}/attachments/dicom/verify-md5'))

	def decode_url(self,url:str) -> Optional[tuple[str,str,str]]:
		r"""
		Split up url into its parts
		:param url: the url to split up
		:return: (host,table,db-id)
		"""
		if ret := _url_matcher.match(url):
			return ret[1], ret[2], ret[3]
		else:
			return None

	def prefix(self) -> str:
		return self.host

	def get_parent(self, entry: Union[str, dict], target_table: str = None) -> dict:
		r"""
		Looks for parents for the given entry.
		Either by returning the direct parent of the given entry or by recursing down the parent tree looking for a specific type
		:param entry: either metadata or path of the child
		:param target_table: the table to look for the parent in ("instances","series" or "studies")
		:return: the path of the parent or None
		"""
		target_map = {"instances": "Instance", "series": "Series", "studies": "Study"}
		meta = self.get_json(entry)
		# break recursion once we found the parent we were looking for or we got nothing
		if not meta or (target_table and target_map[target_table] == meta['Type']):
			return meta

		parent = str()
		if meta['Type'] == "Instance":
			parent = f"/series/{meta['ParentSeries']}"
		elif meta['Type'] == "Series":
			parent = f"/studies/{meta['ParentStudy']}"
		elif meta['Type'] == "Study":
			parent = f"/patients/{meta['ParentPatient']}"

		return self.get_parent(parent, target_table) if target_table else parent

	def _extract_patient_data(self, path: str) -> Optional[str]:
		if meta := self.get_parent(path, "studies"):
			return meta['PatientMainDicomTags']
		else:
			return None

	def lookup(self, table:str, uid) -> Optional[dict]:
		r"""Looks up a DICOM dataset in the DB with the given UID
		:param table: table to look in ("instances", "series" or "studies")
		:param uid: SOPInstanceUID to look up
		:return: dict describing the found element or None if none was found
		"""
		found = get_json(self.fullpath(f"/tools/lookup"), method='post', data=uid)
		if len(found) > 0:
			return found[0]
		else:
			return None

	def lookup_ref(self, ds: pydicom.FileDataset) -> Optional[dict]:
		"""
		Looks up most suitable referenced series by going through "ReferencedImageSequence" tag.
		It will select the series with the highest "SeriesNumber"
		:ds dicom object whose referenced series we're looking for
		:return: metadata of the referenced series if any
		"""
		# look up db-id of referenced instances
		refs = [self.lookup_instance(r.ReferencedSOPInstanceUID) for r in ds.ReferencedImageSequence]
		# get their parent-series's ids
		refs = [self.get_json(r["Path"])['ParentSeries'] for r in refs if r]
		# get parent series sorted by "SeriesNumber"
		refs = sorted([self.get_json(f'/series/{r}') for r in refs], key=lambda r: int(r['MainDicomTags']['SeriesNumber']))
		return refs[-1] if refs else None # return series with highest "SeriesNumber"

	def get_study_name(self, entry: Union[str, dict], separator='_') -> str:
		meta = self.get_json(entry)
		patient_name = meta['PatientMainDicomTags']['PatientName']
		study_date = meta['MainDicomTags']['StudyDate']
		study_time = meta['MainDicomTags']['StudyTime']
		return patient_name + separator + study_date[2:] + '_' + study_time[:6] if study_time and study_date else None

	def get_stdy_timestamp(self, meta: dict) -> datetime:
		from datetime import datetime
		info = meta['MainDicomTags']
		date_str = info['StudyDate']
		time_str = info['StudyTime']
		if date_str and time_str:
			return datetime(int(date_str[0:4]), int(date_str[4:6]), int(date_str[6:8]), int(time_str[0:2]),
			                int(time_str[2:4]), int(time_str[4:6]))
		else:
			return datetime.fromtimestamp(0)

	def download_dicom_files(self, dst_path, instances, write_like_original=True):
		logging.info('Downloading {} files into {}'.format(len(instances), dst_path))
		if not path.isdir(dst_path):
			mkdir(dst_path)
		for instance in instances:
			filename = 'MR.' + self.get_json('/instances/' + instance)['MainDicomTags']['SOPInstanceUID'] + ".ima"
			filename = path.join(dst_path, filename)
			dataset = BytesIO(self.download_file(instance))
			dataset = pydicom.Dataset(pydicom.dcmread(dataset, force=not write_like_original))
			if not write_like_original:
				dataset.ensure_file_meta()
				dataset.file_meta.TransferSyntaxUID = ExplicitVRLittleEndian
				dataset.is_little_endian = True
				dataset.is_implicit_VR = False
				dataset.fix_meta_info()
			pydicom.dcmwrite(filename, dataset=dataset, write_like_original=write_like_original)

	def download_study(self, destination, stdy_id, write_like_original=True):
		dst_path = path.join(destination, self.get_study_name('/studies/' + stdy_id))
		if not path.isdir(dst_path):
			mkdir(dst_path)
		for series in self.get_json('/studies/' + stdy_id + '/series'):
			series_name = 'S{}_{}'.format(series['MainDicomTags']['SeriesNumber'],
			                              series['MainDicomTags']['SeriesDescription'])
			series_path = path.join(dst_path, series_name.replace('/', '_'))
			self.download_dicom_files(series_path, series['Instances'], write_like_original=write_like_original)

	def lookup_ds(self, ds):
		r"""Looks up a DICOM dataset in the DB with the same UID as the given one

		:param host: the url for the orthanc server
		:param ds: bytes, or file-like object containing a DICOM dataset.
		:return: URL (without the host) to the found dataset, None if none was found
		"""
		return self.lookup_instance(ds['SOPInstanceUID'].value)

	def lookup_url(self, url:str):
		r"""Looks up a DICOM dataset in *this* DB with the same UID as the one behind the given url

		:param url: the url for the instance
		:return: URL (without the host) to the found dataset, None if none was found
		"""
		(_, table, _) = get_server_from_url(url).decode_url(url)
		if table[1] == "studies" or table[1] == "series":
			meta = self.get_json(f'/{table}/{id}/instances')[0]
		else:
			meta=get_json(url)
		instance_uid = meta['MainDicomTags']['SOPInstanceUID']
		return self.lookup_instance(instance_uid)

	def list_instances(self, table: str, id:str) -> dict:
		r"""
		Get a dictionary of all instances in the path.

		The returned structure is a dict <SOPInstanceUID>:{
			'ID':<db id of the instance>,
			'ParentSeries':<db id of the instances parent series>,
			'ParentStudy':<db id of the instances parent study>
		}
		:param path: the path to the parent of the instances
		:return: a dictionary of all instances in the study
		"""
		instances = self.get_json(f"/{table}/{id}/instances")
		if not instances: return None
		study_id = self.get_json(f'/series/{instances[0]["ParentSeries"]}')["ParentStudy"]
		return {
			i['MainDicomTags']['SOPInstanceUID']: {
				'ID': i['ID'],
				'ParentSeries': i['ParentSeries'],
				'ParentStudy': study_id
			}
			for i in instances
		}

	def find_parent_for_instance(self, instance_uid: str, target_table: str = None) -> Optional[str]:
		r"""
		Get database id of the parent the given instance is in.
		:param instance_uid: the SOPInstanceUid of the instance
		:param target_table: the table to look for the parent in ("instances","series" or "studies")
		:return: the database id of the study or None
		"""
		if found := self.lookup_instance(instance_uid):
			parent = self.get_parent(found["Path"], target_table=target_table)
			return parent['ID']
		else:
			return None
