import re
from datetime import datetime
from typing import Optional, Union

from requests import get as http_get
from .server import Server
from .core import get_json

_url_matcher = re.compile(r'(http://[^/]+)/api/(\w+)/(.+)', re.IGNORECASE)

def is_rudicom(host: str) -> bool:
	response = http_get(f'{host}/api/info')
	return response.status_code == 200


class Rudicom(Server):

	def decode_url(self,url:str) -> Optional[Union[str,str,str]]:
		r"""
		Split up url into its parts
		:param url: the url to split up
		:return: (host,table,db-id)
		"""
		if ret := _url_matcher.match(url):
			return ret[1], ret[2], ret[3]
		else:
			return None

	def prefix(self) -> str:
		return self.host+"/api"

	def verify_request(self, instance_id: str):
		return http_get(url=self.fullpath(f'/instances/{instance_id}/verify'))

	def lookup(self,table:str, instance_uid:str) -> Optional[dict]:
		r"""Looks up a DICOM dataset in the DB with the given UID
		:param table: table to look in ("instances", "series" or "studies")
		:param uid: SOPInstanceUID to look up
		:return: dict describing the found element or None if none was found
		"""
		# rudicom doesn't need lookup, just check if the instance exists and add "Path" to the answer
		found = self.get_json(f"/{table}/{instance_uid}")
		return {'Path': f'/api/{table}/{instance_uid}'} | found if found else None

	def get_parent(self, path:str, target_table: str = None) -> dict:
		r"""
		Looks for parents for the given entry.
		Either by returning the direct parent of the given entry or by recursing down the parent tree looking for a specific type
		:param entry: either metadata or path of the child
		:param target_table: the table to look for the parent in ("instances","series" or "studies")
		:return: the path of the parent or None
		"""
		level = {"instances": -3, "series": -2, "studies": -1}[target_table]
		parents = self.get_json(f"{path}/parents")
		return parents[level] if parents else None  # last entry is the study

	def _extract_patient_name(self, path:Union[str, dict]) -> Optional[str]:
		meta = self.get_json(path)
		return meta['PatientID'] if meta else None

	def get_study_name(self, path:Union[str, dict], separator='_') -> str:
		meta = self.get_json(path)
		patient_name = meta['PatientID']
		study_date = meta['StudyDate']
		study_time = meta['StudyTime']
		return patient_name + separator + study_date[2:] + '_' + study_time[:6] if study_time and study_date else None

	def get_stdy_timestamp(self, path:Union[str, dict]) -> Optional[datetime]:
		info = self.get_json(path)
		date_str = info['StudyDate']
		time_str = info['StudyTime']
		if date_str and time_str:
			return datetime(int(date_str[0:4]), int(date_str[4:6]), int(date_str[6:8]), int(time_str[0:2]),
			                int(time_str[2:4]), int(time_str[4:6]))
		else:
			return None

	def list_instances(self, table: str, id: str) -> dict:
		r"""
		Get a dictionary of all instances in the path.

		The returned structure is a dict <SOPInstanceUID>:{
			'ID':<db id of the instance>,
			'ParentSeries':<db id of the instances parent series>,
			'ParentStudy':<db id of the instances parent study>
		}
		:param path: the path to the parent of the instances
		:return: a dictionary of all instances in the study
		"""
		instances = self.get_json(f'/{table}/{id}/instances')
		if table=="studies":
			return {i["uid"]: {'ID': i["uid"], 'ParentSeries': i["series"], 'ParentStudy': i["study"]} for i in instances}
		elif table == "series":
			return {i["uid"]: {'ID': i["uid"], 'ParentSeries': i["series"]} for i in instances}
		else:
			raise NameError('Only "studies" or "series" allowed')

	def find_parent_for_instance(self, instance_uid: str, target_table: str = None) -> Optional[str]:
		r"""
		Get database id of the parent the given instance is in.
		:param instance_uid: the SOPInstanceUid of the instance
		:param target_table: the table to look for the parent in ("instances","series" or "studies")
		:return: the database id of the study or None
		"""
		parent = self.get_parent(f'/instances/{instance_uid}', target_table=target_table)
		return parent['uid'] if parent else None
