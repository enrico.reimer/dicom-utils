import logging
import re
from io import BytesIO
from typing import Optional, Union

import pydicom
from pydicom import FileDataset, dcmwrite
from pydicom.errors import InvalidDicomError
from requests import HTTPError, get as http_get
from . import dcm, is_study, get_json

from concurrent.futures import ThreadPoolExecutor


class Server:
	host: str

	def __init__(self, host: str):
		self.host = host

	def __str__(self):
		return self.host

	def get_json(self,path:Union[str, dict]) -> Optional[dict]:
		return path if isinstance(path,dict) else get_json(self.fullpath(path))

	def identify(self, study: Union[str, dict]):
		study = self.get_json(study)
		patient_name = study['PatientMainDicomTags']['PatientName']
		study_date = study['MainDicomTags']['StudyDate']
		study_time = study['MainDicomTags']['StudyTime']
		return f'{patient_name}_{study_date[2:]}_{study_time[:6]}'
	
	def fullpath(self,path) -> str:
		return self.prefix()+path

	def lookup_instance(self, uid) -> Optional[dict]:
		r"""Looks up a DICOM dataset in the DB with the given UID
		:param uid: SOPInstanceUID to look up
		:return: dict describing the found element or None if none was found
		"""
		return self.lookup("instances",uid)

	def upload_file(
			self, f: str, compression: Optional[str] = None,
			subject: Optional[str] = None, description: Optional[str] = None, ignore=[]
	) -> Optional[dict]:
		try:
			logging.debug(f'sending {f} to {self.host}')
			ds = dcm.read(f, compression=compression)
			ds = dcm.anonymize_ds(ds, override_subjectname=subject)

			if description:
				ds['StudyDescription'].value = description

			return self.upload_data(data=ds, source=f, ignore=ignore)
		except InvalidDicomError as e:
			logging.error(f"Failed to read {f} as Dicom ({e})")
		except Exception as e:
			logging.critical(f"Failed uploading {f} ({e})")
		return None

	def transfer(self, server_dst, table: str, id:str, delete: bool, subject_name: str = None, study_id: str = None, description: str = None,
	             ignore=[], force: bool = False):
		path = f"/{table}/{id}"
		if self.host == server_dst.host:
			logging.critical("Source and destination must not be the same server, aborting...")
			return None

		if not subject_name:
			info, detected_name = self.get_subject_info(path)
			if not info:
				logging.critical(f"{self}{path} does not exist or is invalid, won't transfer")
				return None
			if not detected_name:
				logging.warning(f"Invalid subject name found for {self}{path} ({info['PatientName']}), won't transfer")
				return None

		instances_src = self.list_instances(table,id)
		if not instances_src: return

		# look for first src-instance in destination server, and figure out its parent. That most likely is the destination (if it already exists)
		# the parent should have the same table-level as our path (series or study)
		dst_id = server_dst.find_parent_for_instance(next(iter(instances_src)), table)  # iter will generate the keys
		instances_found = server_dst.list_instances(table,dst_id) if dst_id else dict()

		stored = dict()
		if force or delete:  # just transfer all regardless, the transfer below will do a checked delete
			to_be_transferred = instances_src
		else:  # if we don't do transfer on the already existing data we have to explicitly count them as existing
			# sources that are also found in destination
			already_there = {k: instances_found[k] for k in set(instances_src) & set(instances_found)}
			# ids in instances_src that are missing in instances_found
			to_be_transferred = {k: instances_src[k] for k in set(instances_src) - set(instances_found)}

			for instance, data in already_there.items():
				parentstudy = data['ParentStudy']
				stored.setdefault(parentstudy, [0, 0])  # [stored,already_there]
				stored[parentstudy][1] += 1  # (obv. only increase the "already there")
		# todo this should decrease stored[parentstudy][1] above if it fails

		with ThreadPoolExecutor(max_workers=10) as executor:
			trf = lambda instance_id: self.transfer_instance(
				instance_id=instance_id,
				subject_name=subject_name, study_id=study_id,
				description=description,
				destination=server_dst,
				ignore=ignore, delete=delete, force=force
			)
			for store in executor.map(trf, [instance['ID'] for instance in to_be_transferred.values()]):
				if not store: continue
				parentstudy = store['ParentStudy']
				stored.setdefault(parentstudy, [0, 0])  # (stored,already_there)
				if store["Status"] == "Success":
					stored[parentstudy][0] += 1
				elif store["Status"] == "AlreadyStored":
					# instances_found above is not guaranteed to be complete. So this could still happen
					stored[parentstudy][1] += 1
				else:
					logging.error(f'Unexpected result from storing: {store}')

			for study, amount in stored.items():
				logging.info(
					f"{amount[0]} of {len(instances_src)} files from {self.fullpath(path)} where sent to {server_dst.prefix()}/studies/{study} ({amount[1]} where already there)")

	def transfer_instance(self, instance_id, subject_name, description, destination, delete, force, study_id=None, ignore=[]) -> dict:
		logging.debug(f'transferring {self.prefix()}/instances/{instance_id} to {destination}')
		ds = self.download_ds(instance=instance_id)
		ds = dcm.anonymize_ds(ds, override_subjectname=subject_name, override_study_id=study_id)

		if description:
			ds['StudyDescription'].value = description

		uploaded = destination.upload_data(data=ds, source=self.fullpath(f'/instances/{instance_id}'), override=force, ignore=ignore)
		if uploaded and delete:
			get_json(self.fullpath(f'/instances/{instance_id}'), method='DELETE')
		return uploaded

	def find_study(self, regex=None):
		for study in self.get_json('/studies'):
			stdy_path = '/studies/' + study
			stdy_name = self.get_study_name(stdy_path)
			if not regex or re.match(regex, stdy_name):
				yield stdy_name, self.fullpath(stdy_path)

	def download_file(self, instance: str) -> bytes:
		"""
		Download instance with the given id as bytes
		:param instance: the instance id
		:return: the instance data as bytes
		"""
		response = http_get(self.fullpath(f'/instances/{instance}/file'))
		if response.status_code != 200:
			response.raise_for_status()
		else:
			return response.content

	def download_ds(self, instance: str) -> pydicom.FileDataset:
		"""
		Download instance with the given id as dicom object
		:param instance: the instance id
		:return: parsed dicom object of the instance
		"""
		return pydicom.dcmread(BytesIO(self.download_file(instance)))

	def upload_data(self, data: FileDataset, source, override=False, ignore=[]) -> Optional[dict]:
		r"""Sends a new DICOM dataset to the host

		:param data: bytes, or file-like object containing the DICOM dataset.
		:param source: string, to be referred to in error message.
		:param override: replace existing data even if they differ
		:return: json result if data was transferred successfully, None otherwise
		"""

		try:
			buffer = BytesIO()
			dcmwrite(buffer, data)
			buffer.flush()
			buffer.seek(0)
			answer = get_json(self.fullpath('/instances'), method='post', data=buffer)
			if 'Path' not in answer:  # rudicom only replies with Path if there is a duplicate
				answer['Path'] = self.fullpath(f'/instances/{data[0x00080018].value}')  # so sneak in SOPInstanceUID here
			# some info are missing from rudicom answers in all cases, but can be filled in
			if 'ParentStudy' not in answer:
				answer['ParentStudy'] = data[0x0020000D].value

		except ConnectionError as e:
			logging.critical(f'Failed to upload {source} to {self.host}: {e}')
			return None
		except HTTPError as e:
			logging.error(f'Failed to upload {source} to {self.host}: {e}')
			return None

		if answer["Status"] == "Success":  # return answer skipping remaining checks if
			return answer
		elif answer["Status"] == "AlreadyStored":
			if 'AlreadyStored' in answer:  # rudicom returns the existing entry in "AlreadyStored"
				answer['ID'] = answer['AlreadyStored']['uid']  # pluck 'uid' from there
			try:
				duplicate = self.download_ds(instance=answer['ID'])
			except HTTPError as e:
				logging.error(
					f'failed to read already existing {self.host + answer["Path"]} to compare against the upload ({e})')
				return None

			ignore_windowing = [0x00281050, 0x00281055, 0x00281051]
			ignore_anon = [0x00100030, 0x00100010, 0x00100020, 0x00200010]  # birthdate,subjectname,subjectid
			(missing_in_data, diff, missing_in_duplicate) = dcm.difference(data, duplicate,
			                                                               ignore_windowing + ignore_anon + ignore)

			if missing_in_data:
				logging.debug(f'missing in {source}:')
				for m in missing_in_data: logging.debug(m)

			if missing_in_duplicate:
				logging.debug(f'missing in {self.host}{answer["Path"]}:')
				for m in missing_in_duplicate: logging.debug(m)

			if diff:
				logging.debug(f'difference {source}/{self.host}{answer["Path"]}:')
				for d in diff: logging.debug(d)

			if override:
				logging.debug(f"Overriding existing {self.host}{answer['Path']} as requested")
				get_json(f'/{self.host}/{answer["Path"]}', method='DELETE')
				return self.upload_data(data, source=source, ignore=ignore)
			elif not missing_in_data and not diff and not missing_in_duplicate:  # all equal, all good
				return answer
			elif not missing_in_data and not diff:  # some missing in duplicate, but data has all (and no difference)
				logging.info(
					f"Existing {self.host}{answer['Path']} is missing some data, but {source} has all, going to replace it")
				self.get_json(answer['Path'], method='DELETE')
				return self.upload_data(data, source=source, ignore=ignore)
			elif not diff and not missing_in_duplicate:  # some missing in my data, but duplicate has all (and no difference)
				logging.info(
					f"{source} is missing some data, but {self.host}{answer['Path']} has all, going to keep it")
				return answer
			else:
				logging.warning(f'{source} already exists as {self.host}{answer["Path"]} but the data differs')
				for elem in missing_in_data: logging.info(f'{elem} is missing in my data')
				for elem in diff: logging.info(f'{elem[0]}!={elem[1]}')
				for elem in missing_in_duplicate: logging.info(f'{elem} is missing in {self.host}{answer["Path"]}')
		else:
			logging.error(f'Failed to store {source}:{answer["Status"]}')
		return None

	def verify(self, instance_id: str, silent:bool = False) -> bool:
		try:
			response = self.verify_request(instance_id)
		except HTTPError as e:
			if not silent:logging.error(f'Verification of {self.host}/instances/{instance_id} failed with: {e}')
			return False

		if response.status_code != 200:
			if response.status_code == 400:
				if not silent:logging.error(f"Verification of {self.host}/instances/{instance_id} failed with: \"Bad Request\"")
			else:
				answer = response.json()
				if not silent:logging.error(
					f"Verification of {self.host}/instances/{instance_id} failed with: \"{answer['Message']}\"")
			return False
		else:
			return True

	def get_subject_info(self, path:str) -> (dict, str):
		r"""
		Extracts stored and automatically computed subject name if available.
		:param path: the path-part of the url
		:return: tuple of (<actually stored subject info>,automatically selected subject name or None)
		"""
		from dicom_utils import match_4letter, match_code
		info = self._extract_patient_data(path)
		if not info:return None, None
		else: name = info['PatientName']

		# skip code prefixes (like K or L)
		return (info, name[-8:]) if match_code(name[-8:]) else (info, match_4letter(name))
