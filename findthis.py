#!/usr/bin/python3

import argparse
import logging
from requests import delete

from dicom_utils import get_server_from_host, split_url

parser = argparse.ArgumentParser(description='transferring files to orthanc server')
parser.add_argument('source', help='The URLs of the studies to be looked up', nargs='+')
parser.add_argument('-d', '--host', help="the target hosts", nargs="+", default=[
	"http://cameron:2024",
	"http://cameron:3000",
	"http://cameron:4000",
	"http://cameron:5000",
	"http://mri-gateway-nphy:2024",
])
parser.add_argument('--delete', help="delete archived studies", action='store_true', default=False)
parser.add_argument('--log-level', help='logging level', default='info')

args = parser.parse_args()
logging.basicConfig(level=args.log_level.upper())
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

servers = [get_server_from_host(h) for h in args.host]

for url in args.source:
	for server in servers:
		(host,table,id) = split_url(url)
		my_instances = get_server_from_host(host).list_instances(f'/{table}/{id}')
		if found := server.lookup_url(url):
			found = server.get_parent(server.get_parent(found["Path"]))
			other_instances = server.list_instances(found)
			diff = set(other_instances) - set(my_instances)
			if not diff:
				print(f'{server}{found} ({server.identify(found)}) fits ({len(other_instances)} instances)')
				if args.delete:
					logging.info(f'deleting {url} as requested')
					delete(url)
			else:
				print(f'{len(diff)} instances differ in {server}{found} ({server.identify(found)})')
