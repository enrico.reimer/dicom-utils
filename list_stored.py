#!/usr/bin/python3

import datetime
import argparse
import logging
import re
from concurrent.futures import ThreadPoolExecutor

from pynetdicom._globals import BIND_ADDRESS

from dicom_utils.net.dicom_handler import DicomHandler, UnsupportedSOPClassError
from dicom_utils import get_json, get_server_from_host


def skip(data):
	skip_list = [
		r"ServiceImages",
		r"SiemensTest",
		r"Siemens Service",
		r"Service Patient",
		r"warmup_\d{6}_\d{6}",
		r"WARMUP_\d{6}_\d{6}"
	]
	skip_name = str(data["patient_name"])
	if args.filter:
		return not re.match(args.filter, skip_name)
	else:
		for s in skip_list:
			if re.match(s, skip_name):
				return True
	return False


def study_sort_by(item):
	stamp = item[1]['timestamp']
	if isinstance(stamp, datetime.datetime):
		return stamp
	else:
		return datetime.datetime.now()


def identify(data):
	if data["timestamp"]:
		timestamp = data["timestamp"].strftime("%y%m%d_%H%M%S")
		if data["patient_name"].given_name:
			return f'{data["patient_name"].given_name}_{timestamp}'
		else:
			return f'{data["patient_name"]}_{timestamp}'
	else:
		return f'{data["patient_name"]}'


def scanner_collect_series(study_uid):
	ret = {}
	for series_uid, series in dicom_handler.get_series_for_study(study_uid).items():
		series['instances'] = set(dicom_handler.get_instances_for_series(study_uid, series_uid).keys())
		ret[series_uid] = series

	return ret


def series_count_instances(series_map):
	ret = 0
	for series_uid, series in series_map.items():
		ret += len(series['instances'])

	return ret

def db_collect_instances(server, study_id: str) -> dict:
	if found := server.lookup("studies",study_id):
		return server.list_instances("studies",found["ID"])
	else:
		return {}


parser = argparse.ArgumentParser(description='dicom sync')
parser.add_argument('-s', '--scanner', help='the scanners hostname or ip')
parser.add_argument('-p', '--port', type=int, help='the port at the scanner to connect to', default=104)
parser.add_argument('-a', '--scanner_aet', help="the scanners aet")
parser.add_argument('--filter', help="regex filter for study/studies to sync", default=None)
parser.add_argument('-d', '--db', help="the target DB")
parser.add_argument('--local-aet', help="the aet of this side", default='MRI-GATEWAY-1')
parser.add_argument('--log-level', help='logging level (CRITICAL,FATAL,ERROR,WARNING,INFO,DEBUG)', default='warning')
parser.add_argument('--reverse', help="reverse order of processing", action='store_true', default=False)
parser.add_argument("--bind", help="ip and port to bind to (e.g. 127.0.0.1:1040)", default=":0")

args = parser.parse_args()
logging.basicConfig(level=args.log_level.upper())
logging.getLogger('pynetdicom').setLevel('WARNING')
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

bnd = args.bind.split(":")
bind = (bnd[0], int(bnd[1]))

logging.info(f'querying {args.scanner_aet} ({args.scanner}:{args.port}) ...')
dicom_handler = DicomHandler(server=args.scanner, port=args.port, local_ae_title=args.local_aet,
                             bind_address=bind,
                             remote_ae_title=args.scanner_aet)

studies = dicom_handler.get_studies()
studies = sorted(studies.items(), key=lambda x: study_sort_by(x), reverse=args.reverse)
bind = BIND_ADDRESS

server_dst = get_server_from_host(args.db)

logging.info(f'scanning {len(studies)} studies ...')
for study_id, study in studies:
	if skip(study):
		logging.debug(f'Skipping {identify(study)}')
		continue

	ident = identify(study)
	logging.debug(f'Scanning {ident} ..')
	with ThreadPoolExecutor(max_workers=2) as executor:
		db_instances = executor.submit(db_collect_instances, server_dst, study_id)
		scanner_series = executor.submit(scanner_collect_series, study_id)

		try:
			db_instances = db_instances.result().keys()  # all instance UIDs found on the db for this study
			scanner_series = scanner_series.result()
		except Exception as e:
			logging.error(f'Exception {e} when processing {ident}')
			continue

	scanner_instances = 0
	for series_uid, s in scanner_series.items():  # for each series on the scanner
		scanner_instances += len(s['instances']) # count instances on the scanner
		s['instances'] -= db_instances  # remove all instances from it, that exist on the db


	if missing := series_count_instances(scanner_series):
		logging.warning(f'{missing} instances of "{ident}" are missing on {server_dst} ({len(db_instances)} are there)')
	else:
		logging.info(f'"{ident}" is complete on {server_dst} ({scanner_instances}/{len(db_instances)} instances)')

dicom_handler.disconnect()
