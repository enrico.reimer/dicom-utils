#!/usr/bin/python3
import os

from dicom_utils import get_json, get_server_from_url
import argparse
import logging
import re
from os import path, makedirs, link
from pydicom import dcmread, errors as dc_errors

from dicom_utils.orthanc import Orthanc

parser = argparse.ArgumentParser(description='create shadow files for data on orthanc')
parser.add_argument('url', help='url of the target')
parser.add_argument('--log-level', help='logging level (CRITICAL,FATAL,ERROR,WARNING,INFO,DEBUG)', default='info')
parser.add_argument('-r', '--root', help='base directory for link structure', default=path.curdir)
parser.add_argument('--dbroot', help='base directory where the db stores its files',
                    default=path.join(path.curdir, 'orthanc'))
parser.add_argument('--override',
                    help="override existing shadow (will delete and re-create shadow files if they contain the same data)",
                    action='store_true', default=False)

args = parser.parse_args()
logging.basicConfig(level=args.log_level.upper())
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')


def get_patient_path(server: Orthanc, id):
	meta = id if isinstance(id, dict) else get_json(f'{server}/patients/{id}')
	return path.join(args.root, meta['MainDicomTags']['PatientID'])


def get_study_path(server: Orthanc, id):
	meta = id if isinstance(id, dict) else get_json(f'{server}/studies/{id}')
	study_date = meta['MainDicomTags']['StudyDate']
	study_time = meta['MainDicomTags']['StudyTime']
	return path.join(get_patient_path(server, meta['ParentPatient']), study_date[2:] + '_' + study_time[:6])


def make_studies_shadow(server: Orthanc, id):
	for series in get_json(f'{server}/studies/{id}/series'):
		make_series_shadow(server, series)


def make_patients_shadow(server: Orthanc, id):
	for study in get_json(f'{server}/patients/{id}')['Studies']:
		make_studies_shadow(server, study)


def make_link(src, dst):
	logging.info(f'Creating link {src} <= {dst}')
	link(src, dst)


def make_series_shadow(server: Orthanc, id):
	meta = id if isinstance(id, dict) else get_json(f'{server}/series/{id}')
	main_tags = meta['MainDicomTags']
	study_path = get_study_path(server, meta['ParentStudy'])
	series_name = f"S{main_tags['SeriesNumber']}_{main_tags['SeriesDescription']}"
	series_path = path.join(study_path, series_name)
	if not path.isdir(series_path):
		try:
			makedirs(series_path)
		except OSError as e:
			logging.critical(f"Failed to create {series_path}:{e}")

	for instance_id in meta['Instances']:
		try:
			make_shadow(server, instance_id, series_path)
		except dc_errors.InvalidDicomError as e:
			logging.error(f"Failed to create shadow for {instance_id} in {series_path}: {e}")


def make_shadow(server: Orthanc, instance_id, series_path):
	instance_meta = get_json(f'{server}/instances/{instance_id}')
	# filename = response.headers._store['content-disposition'][1][10:-1]
	# file_path = path.join(args.dbroot,filename[0:2],filename[2:4],filename[:-4])
	filename = instance_meta['FileUuid']
	file_path = path.join(args.dbroot, filename[0:2], filename[2:4], filename)
	from_http = server.download_ds(instance_id)
	if not from_http:
		logging.error(f'Failed to load instance {instance_id}')
		return

	from_file = dcmread(file_path) if os.path.exists(file_path) else None
	if not from_file:
		logging.error(f'Failed to load {file_path}')
		return

	if from_file == from_http:  # confirm we got the correct file
		link_name = path.join(series_path, f"{instance_meta['MainDicomTags']['SOPInstanceUID']}.dcm")
		if path.exists(link_name):
			from_link = dcmread(link_name)
			if from_link != from_http:
				logging.error(
					f'Shadow {link_name} already exists, but contains different data than instance {instance_id}')
				return
			elif args.override:
				os.unlink(link_name)
				make_link(file_path, link_name)
			else:
				logging.debug(f'Ignoring already existing link {link_name}')
		else:
			make_link(file_path, link_name)
	else:
		logging.error(f'Database storage {file_path} contains different data than instance {instance_id}')
		return


funcs = {'studies': make_studies_shadow, 'series': make_series_shadow, 'patients': make_patients_shadow}

match = re.match(r'(http://[\w:]+)/(\w+)/([a-f\d]{8}-[a-f\d]{8}-[a-f\d]{8}-[a-f\d]{8}-[a-f\d]{8})', args.url,
                 re.IGNORECASE)
if match:
	(_, tpe, id) = (match[1], match[2], match[3])
	server = get_server_from_url(args.url)
	assert isinstance(server, Orthanc)
	if tpe in funcs:
		funcs[tpe](server=server, id=id)
	else:
		logging.error(f'Unknown type {tpe}')
else:
	logging.critical(f'Failed to recognize url')
