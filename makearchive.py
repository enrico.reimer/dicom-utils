#!/usr/bin/python3

import argparse
import logging
from dicom_utils.archive import store

from dicom_utils.archive import connect
from concurrent.futures import ThreadPoolExecutor

def do_store(url):
	result = store(url, archive_dir=args.apath, suffix=args.suffix, connection=connection, override=args.override)
	if result:
		instances, filename = result
		logging.info(f'{instances} instances from {url} stored in {filename}')
	return filename

parser = argparse.ArgumentParser(description='create an tar.xz archive from a study on orthanc')
parser.add_argument('study', nargs='+')
parser.add_argument('-l', '--login', help='login for the ssh connection to the archive', default="reimer@hsm")
parser.add_argument('--log-level', help='logging level (CRITICAL,FATAL,ERROR,WARNING,INFO,DEBUG)', default='info')
parser.add_argument('--apath', help='archive path', default='/grauserv-ro/raw_data/mr/probands/2024')
parser.add_argument('--suffix', help='archive file suffix', default="")
parser.add_argument('--override', help="override existing archive files", action='store_true', default=False)
parser.add_argument('-k', '--keyfile', help="private rsa keyfile for login", default=None)
parser.add_argument('-p', '--threads', help="amount of worker threads", default=5)
args = parser.parse_args()

logging.basicConfig(level=args.log_level.upper())
logging.getLogger('paramiko.transport').setLevel('WARNING')
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

connection = connect(args.login, keyfile=args.keyfile)

ThreadPoolExecutor(max_workers=args.threads).map(do_store,args.study)

