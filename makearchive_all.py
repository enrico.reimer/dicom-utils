#!/usr/bin/python3

import argparse
import logging
from datetime import datetime, timedelta
from re import fullmatch

from dicom_utils import get_server_from_host
from dicom_utils.archive import store

from dicom_utils.archive import connect
from concurrent.futures import ThreadPoolExecutor


def do_store(url):
	result = store(url, archive_dir=args.apath, suffix=args.suffix, connection=connection, override=args.override)
	if result:
		instances, filename = result
		logging.info(f'{instances} instances from {url} stored in {filename}')
	return filename


parser = argparse.ArgumentParser(description='create an tar.xz archive studies on orthanc')
parser.add_argument('source', help="database to archive")
parser.add_argument('-l', '--login', help='login for the ssh connection to the archive', default="reimer@hsm")
parser.add_argument('--log-level', help='logging level (CRITICAL,FATAL,ERROR,WARNING,INFO,DEBUG)', default='info')
parser.add_argument('--apath', help='archive path', default='/grauserv-ro/raw_data/mr/probands/2024')
parser.add_argument('--suffix', help='archive file suffix', default="")
parser.add_argument('--override', help="override existing archive files", action='store_true', default=False)
parser.add_argument('--filter', help="regex filter for subject names to process", default=r"[\d]{5}\.[\d\w]{2}")
parser.add_argument('--before', help="only process studies older than x days", default=5)
parser.add_argument('-k', '--keyfile', help="private rsa keyfile for login", default=None)
parser.add_argument('-p', '--threads', help="amount of worker threads", default=5)
args = parser.parse_args()

logging.basicConfig(level=args.log_level.upper())
logging.getLogger('paramiko.transport').setLevel('WARNING')
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

connection = connect(args.login, keyfile=args.keyfile)
server = get_server_from_host(args.source)

to_do = filter(
	lambda s: fullmatch(args.filter, s['PatientMainDicomTags']['PatientName']),
	server.get_json('/studies?expand')
)
to_do = filter(lambda s: server.get_stdy_timestamp(s) < datetime.now() - timedelta(days=int(args.before)), to_do)
to_do = [f'{server}/studies/{s["ID"]}' for s in to_do]

ThreadPoolExecutor(max_workers=args.threads).map(do_store, to_do)
