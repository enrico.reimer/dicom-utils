#!/usr/bin/python3

import argparse
import logging

from pydicom import FileDataset
from dicom_utils import get_server_from_host, get_json
from dicom_utils.dcm import anonymize_ds

parser = argparse.ArgumentParser(description='restore retro-reconstructed data to its rightfull study by looking up StudyInstanceUID of its ReferencedSOPInstanceUID')
parser.add_argument('series', help='the url of the series to be processed', nargs='+')
parser.add_argument('--offset', type=int, default=100)
parser.add_argument('-d', '--host', default='http://cameron:2024')
parser.add_argument('--log-level', help='logging level', default='info')
parser.add_argument('--delete', help="delete source data after they have been transferred (or are already on the db)",
                    action='store_true', default=False)

args = parser.parse_args()
logging.basicConfig(level=args.log_level.upper())
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

for srs in args.series:
    source = get_server_from_host(src_host)
    dest = get_server_from_host(args.host)

    # first get a list of the instances at the source
    instances=source.list_instances(f'/series/{src_srs}')
    # get a sample (just pick the first instance)
    sample=source.download_ds(list(instances.values())[0]['ID'])
    # look up its referenced series in dest
    looked_up_series = dest.lookup_ref(sample)
    if not looked_up_series:
        logging.error(f"couldn't find a reference for {srs} in {dest}, skipping.. ")
        continue

    looked_up_study = dest.get_json(f'/studies/{looked_up_series["ParentStudy"]}')
    logging.debug(f'found original series in {dest}/series/{looked_up_series["ID"]}')
    result = {"Success":0,"AlreadyStored":0}
    sn = sample.SeriesNumber+args.offset
    for instance in instances.values():
        ds = source.download_ds(instance['ID'])
        ds.StudyInstanceUID = looked_up_study['MainDicomTags']['StudyInstanceUID']
        ds.SeriesNumber = sn
        ds = anonymize_ds(ds)

        ds[0x00100010].value = looked_up_study['PatientMainDicomTags']['PatientName']  # patient name
        ds[0x00100020].value = looked_up_study['PatientMainDicomTags']['PatientID']  # patient id

        if uploaded := dest.upload_data(data=ds, source=f'{source}/instances/{instance["ID"]}',override=True):
            if args.delete:
                source.get_json(f'/instances/{instance["ID"]}', method='DELETE')

            key = uploaded['Status']
            result[key]+=1

    looked_up_study_name = dest.get_study_name(looked_up_study)
    logging.info(f'{result["Success"]} out of {len(instances.keys())} instances from {source}/series/{src_srs} transferred to series #{sn} in {looked_up_study_name} ({dest}/studies/{looked_up_study["ID"]})')
