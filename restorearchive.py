from dicom_utils.archive import connect, find_regex_in_archive
from argparse import ArgumentParser
import logging
from pathlib import Path
from pydicom import dcmwrite
from dicom_utils import get_server_from_host

search_paths = [f'/grauserv-ro/raw_data/mr/probands/{i}' for i in range(2024, 2010, -1)]

parser = ArgumentParser(description='download data from the archive')
parser.add_argument('name', help='regex to search for')
parser.add_argument('--host', help='download into orthanc db')
parser.add_argument('-d', '--dest', help='download into destination path', type=Path)
parser.add_argument('-l', '--login', help='login for the ssh connection to the archive', default="reimer@hsm")
parser.add_argument('--apath', nargs="+", help='archive search paths', default=search_paths)
parser.add_argument('--log-level', help='logging level (CRITICAL,FATAL,ERROR,WARNING,INFO,DEBUG)', default='info')
parser.add_argument('--ignore', help='dicom tags to ignore when comparing db to archive', default=[
	"StudyDescription", "PatientName", "PatientID", "PatientBirthDate", "WindowCenter", "WindowWidth",
	(0x0028, 0x1055),
	(0x0038, 0x0010),
	(0x0029, 0x1210),
	(0x0029, 0x1110)  # MedCom OOG Info
])

args = parser.parse_args()
logging.basicConfig(level=args.log_level.upper())
logging.getLogger('paramiko.transport').setLevel('WARNING')
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

connection = connect(args.login)
found = find_regex_in_archive(search_string=args.name, paths=args.apath, connection=connection)

if not found: exit(0)

if args.host:
	for (a_tarinfo, a_ds) in found:
		server = get_server_from_host(args.host)
		server.upload_data(a_ds, source="archive", ignore=args.ignore)
elif args.dest:
	for (a_tarinfo, a_ds) in found:
		file = Path(args.dest).joinpath(a_tarinfo.path)
		if not file.parent.is_dir(): file.parent.mkdir(parents=True)
		logging.info(file)
		dcmwrite(file, a_ds)
