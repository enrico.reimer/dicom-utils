#!/usr/bin/python3
import datetime
import logging
import argparse
from dicom_utils import get_server_from_host
from requests import delete

def timestamp(study: dict) -> datetime:
	return datetime.strptime(study['LastUpdate'], "%Y%m%dT%H%M%S")

parser = argparse.ArgumentParser(description='delete all studies above a certain age on an orthanc server')
parser.add_argument('--host', help='orthanc db host', default='http://nasenbaer:8042')
parser.add_argument('--log-level', help='logging level (CRITICAL,FATAL,ERROR,WARNING,INFO,DEBUG)', default='info')
parser.add_argument('--delete', help="actually delete studies", action='store_true', default=False)
parser.add_argument('--age', help="minimal age in days", default=40, type=int)

args = parser.parse_args()
logging.basicConfig(level=args.log_level.upper())
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

cutoff = datetime.now() - datetime.timedelta(days=args.age)
server = get_server_from_host(args.host)
studies = filter(lambda s: timestamp(s) < cutoff, server.get_json(f'{args.host}/studies?expand'))

for stdy in sorted(studies, key=timestamp):
	if args.delete:
		logging.info(f'deleting {server.identify(stdy)} (last updated {timestamp(stdy)})')
		delete(f'{server}/studies/{stdy["ID"]}')
	else:
		print(f'would delete {server.identify(stdy)} (last updated {timestamp(stdy)})')
