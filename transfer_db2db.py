#!/usr/bin/python3

# This does transfer using storescu and checks the result with isisdiff
import argparse
import logging
from dicom_utils import get_server_from_host, get_server_from_url

skipped = 0

if __name__ == "__main__":

	# parse commandline
	parser = argparse.ArgumentParser(description='transferring files to orthanc server')
	parser.add_argument('source', help='The URLs of the studies to be transferred', nargs='+')
	parser.add_argument('--subject', default=None,
	                    help='Override automatic subject name generation (this also disables filtering of subject names)')
	parser.add_argument('--study', default=None,
	                    help='Override automatic study-id generation (this also disables filtering of study ids)')
	parser.add_argument('-d', '--host', help="the target host", default='http://cameron:2024')
	parser.add_argument('--log-level', help='logging level', default='info')
	parser.add_argument('--ignore', nargs='*', default=[],
	                    help='dicom parameters to ignore when checking for differences with existing data')
	parser.add_argument('--delete', action='store_true', default=False,
	                    help="delete source data after they have been transferred (or are already on the db)")
	parser.add_argument('--force', help="force transfer even if data differ", action='store_true', default=False)
	parser.add_argument('--description', help="override study description with given text", default=None)

	args = parser.parse_args()
	logging.basicConfig(level=args.log_level.upper())
	logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

	server_dst = get_server_from_host(args.host)

	for source in args.source:
		server_src = get_server_from_url(source)
		(_, table, stdy_id) = server_src.decode_url(source)
		server_src.transfer(server_dst,
		                    table=table,id=stdy_id,
		                    subject_name=args.subject, study_id=args.study,
		                    description=args.description,
		                    ignore=args.ignore, delete=args.delete, force=args.force
		                    )
