#!/usr/bin/python3

# This does transfer using storescu and checks the result with isisdiff
import argparse
import logging
from datetime import datetime, timedelta
from re import match

from dicom_utils import get_server_from_host, is_study

# parse commandline
parser = argparse.ArgumentParser(description='transferring files to orthanc server')
parser.add_argument('-s', '--source', help='the source database to pull data from', default='http://cameron:8042')
parser.add_argument('--before', help='Process only data that are at least n days old', default=0)
parser.add_argument('--log-level', help='logging level', default='warning')
parser.add_argument('--delete', help="delete source after transfer", action='store_true', default=False)

args = parser.parse_args()
logging.basicConfig(level=args.log_level.upper())
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

whitelist = {
	r"(QA)_\d+(_GUFI_32ch_ALT)": "http://cameron:3000",
	r"(QA)_\d+(_GUFI_64ch_ALT)": "http://cameron:3000",
	r"(QA)_\d+(_GUFI_32ch_Phantom7T)": "http://cameron:3000",
	r"(QA)_\d+(_GUFI_64ch_Phantom7T)": "http://cameron:3000",
	r"(QA)_\d+(_GUFI_32ch)": "http://cameron:3000",
	r"(QA)_\d+(_GUFI_64ch)": "http://cameron:3000",
	r"(QA)_\d+(_Diffusion)": "http://cameron:3000",
	r"(QA)_\d+(_[ps]TX_32ch)": "http://cameron:3000",
	r"(Phantom_chuy).*": "http://cameron:4000",
	r"(Phantom_trampel).*": "http://cameron:4000",
	r"(Phantom_ledwards).*": "http://cameron:4000",
	r"(Phantom_kpine).*": "http://cameron:4000",
	r"(Phantom_jschmidt).*": "http://cameron:4000",
	r"(Phantom_dchaimow).*": "http://cameron:4000",
	r"(Phantom_mildner).*": "http://cameron:4000",
}


def auto_dest(server, path: str):
	"""
	Figure out subject name and target DB from stored subject name.
	:param server: source DB to talk to
	:param path: source path
	:return: a tuple of the detected subject name and the http-address of the target DB or None if detection failed
	"""
	# first try automatic detection (subject id)
	stored, detected = server.get_subject_info(path)
	if detected:  # if ok target will be default
		study_id = stored['PatientID']
		if "PHY" in study_id:  # "PHY" is always neurophysics
			return detected, "http://cameron:2024"
		else:
			if is_study(study_id):
				return detected, "http://mri-gateway-nphy:2024"
			else:
				logging.debug(f"study id '{study_id}' not found")

	# whitelist fallback
	for (k, v) in whitelist.items():
		m = match(k, stored['PatientName'])
		if m: return "".join(m.groups()), v
	return None


server_src = get_server_from_host(args.source)
to_do = {s["ID"]: s | {"timestamp": server_src.get_stdy_timestamp(s)} for s in server_src.get_json('/studies?expand')}

for stdy in sorted(to_do, key=lambda id: to_do[id]["timestamp"]):
	if args.before and to_do[stdy]["timestamp"] > datetime.now() - timedelta(int(args.before)):
		continue

	if to_do[stdy]["timestamp"] < datetime(2024, 1, 1, 0, 0, 0):
		continue

	stored = dict()
	source_path = f'/studies/{stdy}'

	if dest := auto_dest(server=server_src, path=source_path):
		try:
			server_dst = get_server_from_host(dest[1])
		except ConnectionError as e:
			logging.error(f'Failed connecting to destination server via {dest}, skipping ...')
			continue

		logging.debug(f'Sending {server_src.fullpath(source_path)} to {server_dst}')
		server_src.transfer(server_dst, table="studies", id=stdy, subject_name=dest[0], delete=args.delete)
	else:
		info = server_src.get_json(source_path)
		logging.warning(
			f"Couldn't automatically find destination  for {server_src.fullpath(source_path)} ({server_src.identify(info)} / StudyID: {info['PatientMainDicomTags']['PatientID']}), won't transfer"
		)
		continue
