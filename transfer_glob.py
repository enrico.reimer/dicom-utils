#!/usr/bin/python3

import argparse
import glob
import logging
from concurrent.futures import ThreadPoolExecutor
from os import unlink as delete, path
from typing import Optional

from dicom_utils import get_server_from_host


def transfer_file(f: str) -> (str, Optional[dict]):
	return f, server.upload_file(f, compression=args.compression,
		subject=args.subject,
		description=args.description,
		ignore=args.ignore
	)


# parse commandline
parser = argparse.ArgumentParser(description='transferring files to orthanc server')
parser.add_argument('path', help='The files to process (this will be used for glob => "**/*.ima" will work)', nargs='+')
parser.add_argument('-d', '--host', default='http://cameron:2024')
parser.add_argument('--log-level', help='logging level', default='warning')
parser.add_argument('--ignore', nargs='*', help='additional attributes to ignore', default=[])
parser.add_argument('--compression', help='if the files need to be uncompressed ("gz", "orthanc")', default=None)
parser.add_argument('--delete', help="delete source files after they have been transferred (or are already on the db)",
                    action='store_true', default=False)
parser.add_argument('--subject', help="override subject name with given text", default=None)
parser.add_argument('--description', help="override study description with given text", default=None)

args = parser.parse_args()
logging.basicConfig(level=args.log_level.upper())
logging.getLogger('urllib3.connectionpool').setLevel('WARNING')

server = get_server_from_host(args.host)

for g in args.path:
	skipped = 0
	stored = dict()
	files = glob.iglob(g, recursive=True)
	files = list(filter(lambda f: not path.isdir(f), files))
	upload = {}
	logging.info(f"Found {len(files)} files...")
	with ThreadPoolExecutor(max_workers=10) as executor:
		for f, result in executor.map(transfer_file, files):
			if result:
				if args.delete: delete(f)
				if not result['ParentStudy'] in stored:
					stored[result['ParentStudy']] = [0, 0]
				if result["Status"] == "Success":
					stored[result['ParentStudy']][0] += 1
				elif result["Status"] == "AlreadyStored":
					stored[result['ParentStudy']][1] += 1
			else:
				skipped += 1

	for stdy, amount in stored.items():
		if amount[0]: logging.info(f"{amount[0]} files where stored in {args.host}/studies/{stdy} from {g}")
		if amount[1]: logging.info(f"{amount[1]} files from {g} already existed in {args.host}/studies/{stdy}")
	if skipped:
		logging.warning(f"{skipped} files from {g} where not stored")
